﻿using Common;
using Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Homework;

internal static class Program
{
    private static void Main(string[] args)
    {
        var provider = DependencyInjection.ConfigureServices();
        var service = provider.GetRequiredService<IArraySumService>();
        service.Process();
    }
}