﻿namespace Common.Interfaces;

public interface ICalcArraySumService
{
    long Calc(int[] array);

    string GetServiceType();
}