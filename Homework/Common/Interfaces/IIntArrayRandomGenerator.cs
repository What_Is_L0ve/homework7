﻿namespace Common.Interfaces;

public interface IIntArrayRandomGenerator
{
    int[] Generate(int length);
}